import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import {Button} from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import LoginScreen from './screens/Login'
import SignupScreen from './screens/Signup'
import HomeScreen from './screens/Home'
import CadetScreen from './screens/Cadet'


const Stack = createNativeStackNavigator();


export default function App() {
  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Signup" component={SignupScreen} />
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Cadet" component={CadetScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}







