import { StatusBar } from 'expo-status-bar';
import React, { useState,useEffect } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { NativeBaseProvider, Button, Input, Icon, Center, Image, KeyboardAvoidingView } from 'native-base';
import { MaterialIcons } from "@expo/vector-icons"
import { StackActions } from '@react-navigation/native';
import { initializeApp } from 'firebase/app';
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyCKT8Ovrwtl9yJBJ4YZMI4T-L2tBtFWGJM",
    authDomain: "cs4205-crma.firebaseapp.com",
    databaseURL: "https://cs4205-crma.firebaseio.com",
    projectId: "cs4205-crma",
    storageBucket: "cs4205-crma.appspot.com",
    messagingSenderId: "823706870912",
    appId: "1:823706870912:web:98950b650c1fc559187265"
};

const app = initializeApp(firebaseConfig);

const auth = getAuth();


export default function App({ navigation }) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const inputEmailChange = (text) => {
        console.log(text);
        setEmail(text);
    }
    const inputPasswordChange = (text) => {
        console.log(text);
        setPassword(text);
    }
    useEffect(()=>{
        const user = auth.currentUser;
        if(user){
            navigation.dispatch(
                StackActions.replace('Home')
            );
        }

    },[])

    return (
        <NativeBaseProvider>
            <KeyboardAvoidingView
                h={{
                    base: "400px",
                    lg: "auto",
                }}
                behavior={Platform.OS === "ios" ? "padding" : "height"}
            >
                <View style={styles.container}>
                    <Image
                        style={{marginTop:120}}
                        source={require('../assets/logo.jpeg')}
                        alt="Alternate Text"
                        size="xl"
                    />
                    <Text style={{ color: 'orange', fontSize: 30, fontWeight: 'bold', margin: 30 }}>{'Cs4205'}</Text>
                    <Input
                        w={{ base: "75%", md: "25%", }}
                        mb={3}
                        onChangeText={inputEmailChange}
                        InputLeftElement={
                            <Icon
                                as={<MaterialIcons name="person" />}
                                size={5}
                                ml="2"
                                color="muted.400"
                            />
                        }
                        placeholder="Email"
                    />
                    <Input
                        w={{ base: "75%", md: "25%", }}
                        mb={3}
                        onChangeText={inputPasswordChange}
                        InputLeftElement={
                            <Icon
                                as={<MaterialIcons name="lock" />}
                                size={5}
                                ml="2"
                                color="muted.400"
                            />
                        }
                        placeholder="Password"
                    />
                    <Button style={{ backgroundColor: '#fb923c' }} w={{ base: "75%", md: "25%", }} mb={3} onPress={() => {

                        signInWithEmailAndPassword(auth, email, password)
                            .then((userCredential) => {
                                // Signed in 
                                const user = userCredential.user;
                                navigation.dispatch(
                                    StackActions.replace('Home')
                                );
                            })
                            .catch((error) => {
                                const errorCode = error.code;
                                const errorMessage = error.message;
                                alert(errorMessage);
                            });
                    }}>
                        {'Log in'}
                    </Button>
                    <Button style={{ backgroundColor: '#292524' }} w={{ base: "75%", md: "25%", }} onPress={() => {
                        navigation.navigate('Signup');
                    }}>
                        {'Sign up'}
                    </Button>
                    <StatusBar style="auto" />
                </View>
            </KeyboardAvoidingView>
        </NativeBaseProvider>
    );
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
