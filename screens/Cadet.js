import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { NativeBaseProvider, Button, Input, Icon, Center, Image, KeyboardAvoidingView } from 'native-base';
import { MaterialIcons } from "@expo/vector-icons"
import { getFirestore, collection, addDoc } from "firebase/firestore";

const db = getFirestore();


export default function App({ navigation }) {

    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [service, setService] = useState('');

    const inputNameChange = (text) => {
        console.log(text);
        setName(text);
    }
    const inputPhoneChange = (text) => {
        console.log(text);
        setPhone(text);
    }
    const inputServiceChange = (text) => {
        console.log(text);
        setService(text);
    }
    const addCadet = async () => {
        try {
            const docRef = await addDoc(collection(db, "Cadet"), {name,phone,service});
            console.log("Document written with ID: ", docRef.id);
            alert('Success')
            navigation.goBack();
        } catch (e) {
            console.error("Error adding document: ", e);
        }

    }

    return (
        <NativeBaseProvider>
            <KeyboardAvoidingView
                h={{
                    base: "400px",
                    lg: "auto",
                }}
                behavior={Platform.OS === "ios" ? "padding" : "height"}
            >
                <View style={styles.container}>
                    
                    <Text style={{ color: 'orange', fontSize: 30, fontWeight: 'bold', margin: 30 }}>{'Add Cadet Info'}</Text>
                    <Input
                        w={{ base: "75%", md: "25%", }}
                        mb={3}
                        onChangeText={inputNameChange}
                        InputLeftElement={
                            <Icon
                                as={<MaterialIcons name="person" />}
                                size={5}
                                ml="2"
                                color="muted.400"
                            />
                        }
                        placeholder="Name"
                    />
                    <Input
                        w={{ base: "75%", md: "25%", }}
                        mb={3}
                        onChangeText={inputPhoneChange}
                        InputLeftElement={
                            <Icon
                                as={<MaterialIcons name="phone" />}
                                size={5}
                                ml="2"
                                color="muted.400"
                            />
                        }
                        placeholder="Phone"
                    />
                    <Input
                        w={{ base: "75%", md: "25%", }}
                        mb={3}
                        onChangeText={inputServiceChange}
                        InputLeftElement={
                            <Icon
                                as={<MaterialIcons name="star" />}
                                size={5}
                                ml="2"
                                color="muted.400"
                            />
                        }
                        placeholder="Service"
                    />
                    <Button style={{ backgroundColor: '#fb923c' }} w={{ base: "75%", md: "25%", }} mb={3} onPress={() => {
                        addCadet();
                    }}>
                        {'Create Contact'}
                    </Button>
                    
                    <StatusBar style="auto" />
                </View>
            </KeyboardAvoidingView>
        </NativeBaseProvider>
    );
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
});
