import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { NativeBaseProvider, Button, Input, Icon, Center, Image ,KeyboardAvoidingView} from 'native-base';
import { MaterialIcons } from "@expo/vector-icons"
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";

const auth = getAuth();


export default function App({ navigation }) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const inputEmailChange = (text) => {
        console.log(text);
        setEmail(text);
    }
    const inputPasswordChange = (text) => {
        console.log(text);
        setPassword(text);
    }

    return (
        <NativeBaseProvider>
            <KeyboardAvoidingView
                h={{
                    base: "400px",
                    lg: "auto",
                }}
                behavior={Platform.OS === "ios" ? "padding" : "height"}
            >
            <View style={styles.container}>
                <Image
                    style={{marginTop:120}}
                    source={require('../assets/logo.jpeg')}
                    alt="Alternate Text"
                    size="xl"
                />
                <Text style={{ color: 'orange', fontSize: 30, fontWeight: 'bold', margin: 30 }}>{'Sign Up'}</Text>
                <Input
                    w={{ base: "75%", md: "25%", }}
                    mb={3}
                    onChangeText={inputEmailChange}
                    InputLeftElement={
                        <Icon
                            as={<MaterialIcons name="person" />}
                            size={5}
                            ml="2"
                            color="muted.400"
                        />
                    }
                    placeholder="Email"
                />
                <Input
                    w={{ base: "75%", md: "25%", }}
                    mb={3}
                    onChangeText={inputPasswordChange}
                    InputLeftElement={
                        <Icon
                            as={<MaterialIcons name="lock" />}
                            size={5}
                            ml="2"
                            color="muted.400"
                        />
                    }
                    placeholder="Password"
                />
                <Button style={{ backgroundColor: '#fb923c' }} w={{ base: "75%", md: "25%", }} mb={3} onPress={() => {
                    createUserWithEmailAndPassword(auth, email, password)
                        .then((userCredential) => {
                            // Signed in 
                            const user = userCredential.user;
                            navigation.goBack();
                        })
                        .catch((error) => {
                            const errorCode = error.code;
                            const errorMessage = error.message;
                            alert(errorMessage);
                        });
                }}>
                    {'Sign up'}
                </Button>
                <Button style={{ backgroundColor: '#292524' }} w={{ base: "75%", md: "25%", }} onPress={() => {
                    navigation.goBack();
                }}>
                    {'Back'}
                </Button>
                <StatusBar style="auto" />
            </View>
            </KeyboardAvoidingView>
        </NativeBaseProvider>
    );
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
